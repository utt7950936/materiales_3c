from django.contrib import admin

from .models import *

# Register your models here.

# se le pone el punto(.) para indicar que esta en el mismo folfer


@admin.register(Clientes)
class ClientesAdmin(admin.ModelAdmin):
    list_display = [
        "id_cliente",
        "nombre",
        "telefono",
        "correo"
    ]

@admin.register(Lugar)
class LugarAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "num_calle",
        "colonia",
        "codigo_postal"
    ]

@admin.register(Tipos)
class TiposAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "nombre",
        "descripcion"
    ]


