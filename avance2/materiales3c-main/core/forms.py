from django import forms

from .models import *


class ClientesForm(forms.ModelForm):
    class Meta:
        model = Clientes
        fields = "__all__"
        exclude = ["id_cliente"]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del cliente"}),
            "telefono": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el número de teléfono"}),
            "correo": forms.EmailInput(attrs={"type": "email", "class": "form-control", "placeholder": "Ingrese el correo electrónico"}),
        }

class UpdateClientesForm(forms.ModelForm):
    class Meta:
        model = Clientes
        fields = "__all__"
        exclude = ["ID_Cliente"]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del cliente"}),
            "telefono": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el número de teléfono"}),
            "correo": forms.EmailInput(attrs={"type": "email", "class": "form-control", "placeholder": "Ingrese el correo electrónico"}),
        }

class LugarForm(forms.ModelForm):
    class Meta:
        model = Lugar
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "num_calle": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el número de calle"}),
            "colonia": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre de la colonia"}),
            "codigo_postal": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el código postal"}),
        }

class UpdateLugarForm(forms.ModelForm):
    class Meta:
        model = Lugar
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "num_calle": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el número de calle"}),
            "colonia": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre de la colonia"}),
            "codigo_postal": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el código postal"}),
        }

class TiposForm(forms.ModelForm):
    class Meta:
        model = Tipos
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del tipo"}),
            "descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la descripción del tipo"}),
        }

class UpdateTiposForm(forms.ModelForm):
    class Meta:
        model = Tipos
        fields = "__all__"
        exclude = ["codigo"]
        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese el nombre del tipo"}),
            "descripcion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Ingrese la descripción del tipo"}),
        }