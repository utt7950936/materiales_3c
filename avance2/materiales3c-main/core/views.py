from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic

from .forms import *
from .models import *

# Create your views here.

##Estadio

class ListClientes(generic.View):
    template_name = "core/list_Clientes.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Clientes.objects.all()
        self.context = {
            "Clientes": queryset  
        }
        return render(request, self.template_name, self.context)



class DetailClientes(generic.DetailView):
    template_name = "core/detailCliente.html"
    model = Clientes


class createCliente(generic.CreateView):
    template_name = "core/createCliente.html"
    model = Clientes
    form_class = ClientesForm
    success_url = reverse_lazy("core:List_Clientes")


class UpdateCliente(generic.UpdateView):
    template_name = "core/UpdateCliente.html"
    model = Clientes
    form_class = UpdateClientesForm
    success_url = reverse_lazy("core:List_Clientes")

class DeleteCliente(generic.DeleteView):
    template_name = "core/deleteCliente.html"
    model = Clientes
    success_url = reverse_lazy("core:List_Clientes")


##Equipos

class ListTipo(generic.View):
    template_name = "core/listTipo.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Tipos.objects.all()
        self.context = {
            "Tipos": queryset
        }
        return render(request, self.template_name,self.context)
    
class CreateTipo(generic.CreateView):
    template_name = "core/create_tipo.html"
    model = Tipos
    form_class = TiposForm
    success_url = reverse_lazy("core:list_tipos")

    def form_valid(self, form):
        # Aquí puedes realizar operaciones adicionales si es necesario
        return super().form_valid(form)




class DetailTipo(generic.DetailView):
    template_name = "core/detailTipo.html"
    model = Tipos

class UpdateTipo(generic.UpdateView):
    template_name = "core/UpdateTipo.html"
    model = Tipos
    form_class = UpdateTiposForm
    success_url = reverse_lazy("core:list_tipos")

class DeleteTipo(generic.DeleteView):
    template_name = "core/deleteTipo.html"
    model = Tipos
    success_url = reverse_lazy("core:list_tipos")

##Ciudades

class ListLugar(generic.View):
    template_name = "core/listLugar.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Lugar.objects.all()
        self.context = {
            "Lugar": queryset
        }
        return render(request, self.template_name,self.context)

class CreateLugar(generic.CreateView):
    template_name = "core/createLugar.html"
    model = Lugar
    form_class = LugarForm
    success_url = reverse_lazy("core:listLugar")

class DetailLugar(generic.DetailView):
    template_name = "core/detailLugar.html"
    model = Lugar

class UpdateLugar(generic.UpdateView):
    template_name = "core/UpdateLugar.html"
    model = Lugar
    form_class = UpdateLugarForm
    success_url = reverse_lazy("core:listLugar")

class DeleteLugar(generic.DeleteView):
    template_name = "core/deleteLugar.html"
    model = Lugar
    success_url = reverse_lazy("core:listLugar")
    
    

