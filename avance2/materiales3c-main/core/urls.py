from django.urls import path

from core import views

from .models import *

app_name = "core"
urlpatterns = [
##Clientes
path('list/clientes/', views.ListClientes.as_view(), name="List_Clientes"),
path('create/cliente/', views.createCliente.as_view(), name="createCliente"),
path('detail/cliente/<int:pk>/', views.DetailClientes.as_view(), name="detailCliente"),
path('update/cliente/<int:pk>/', views.UpdateCliente.as_view(), name="updateCliente"),
path('delete/cliente/<int:pk>/', views.DeleteCliente.as_view(), name="deleteCliente"),

##Tipos
path('list/tipos/', views.ListTipo.as_view(), name="list_tipos"),
path('create/tipo/', views.CreateTipo.as_view(), name="create_tipo"),
path('detail/tipo/<int:pk>/', views.DetailTipo.as_view(), name="detail_tipo"),
path('update/tipo/<int:pk>/', views.UpdateTipo.as_view(), name="update_tipo"),
path('delete/tipo/<int:pk>/', views.DeleteTipo.as_view(), name="delete_tipo"),

##Lugar
path('list/lugar/', views.ListLugar.as_view(), name="listLugar"),
path('create/lugar/', views.CreateLugar.as_view(), name="create_lugar"),
path('detail/lugar/<int:pk>/', views.DetailLugar.as_view(), name="detail_lugar"),
path('update/lugar/<int:pk>/', views.UpdateLugar.as_view(), name="update_lugar"),
path('delete/lugar/<int:pk>/', views.DeleteLugar.as_view(), name="delete_lugar"),



]