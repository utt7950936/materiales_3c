from django.contrib.auth.models import User
from django.db import models

# Create your models here.



class Clientes(models.Model):
    id_cliente = models.AutoField(primary_key=True, db_column='ID_Cliente')  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=255, blank=True, null=True)  # Field name made lowercase.
    telefono = models.CharField(max_length=255, blank=True, null=True)
    correo = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Clientes'


class Lugar(models.Model):
    codigo = models.AutoField(primary_key=True)
    num_calle = models.CharField(max_length=10)
    colonia = models.CharField(max_length=20)
    codigo_postal = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'lugar'


class Tipos(models.Model):
    codigo = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=20)
    descripcion = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'tipos'








